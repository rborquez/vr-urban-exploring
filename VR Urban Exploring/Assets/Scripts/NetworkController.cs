﻿using UnityEngine;
using System.Collections;

public class NetworkController : MonoBehaviour
{
	string _room = "VR Urban Exploring";


	void Start()
	{
		PhotonNetwork.ConnectUsingSettings("0.1");
		var temp = PhotonVoiceNetwork.Client;
	}

	void OnJoinedLobby()
	{
		Debug.Log("joined lobby");
		RoomOptions roomOptions = new RoomOptions() { };
		PhotonNetwork.JoinOrCreateRoom(_room, roomOptions, TypedLobby.Default);
	}

	void OnJoinedRoom()
	{
		PhotonNetwork.Instantiate("NetworkedPlayer", Vector3.zero, Quaternion.identity, 0);
	}

	void onJoinedVoiceRoom()
	{
		Debug.Log ("Joined voice room");
	}
}
