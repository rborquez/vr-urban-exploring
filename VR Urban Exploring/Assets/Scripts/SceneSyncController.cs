﻿using UnityEngine;
using System.Collections;

public class SceneSyncController : MonoBehaviour {
	public Canvas InstructionCanvas;
	public GameObject PlayerController, Pivot, Sphere;
	public MediaPlayerCtrl srcMedia;

	//This makes the Sphere Video Player invisible to the players until they press the Sync Button
	void Start (){
		//PhotonNetwork.Instantiate("NetworkedPlayer", Vector3.zero, Quaternion.identity, 0);
		Sphere.SetActive (false);
	}
	//This creates the signalling object and disables the instruction canvas when the sync button is pressed
	public void OnSyncPress () {
		InstructionCanvas.GetComponent<Canvas> ().enabled = false;
		PhotonNetwork.Instantiate(Application.loadedLevelName, Vector3.zero, Quaternion.identity, 0);
		Update();
	}
	//This checks for the above object, enables the sphere, moves the player into the video and plays it.
	void Update(){
		if(GameObject.Find("RoofScene(Clone)") != null){
			Sphere.SetActive (true);
			PlayerController.transform.position = new Vector3 (0, 2, 0);
		}
		if(GameObject.Find("BMXScene(Clone)") != null){
			Sphere.SetActive (true);
			PlayerController.transform.position = new Vector3 (0, 2, 0);
		}
	}
}
