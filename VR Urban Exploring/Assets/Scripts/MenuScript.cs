using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {
	//This function takes the name of a video as an argument and creates a matching
	//gameobject which is instantiated on every player's game. The players read
	//this object in Update() and launch the appropriate video. We did it this way
	//because of endless bugs with RPCs and other official communication methods.
	public void LoadScene (string destination) {
		PhotonNetwork.Instantiate(destination, Vector3.zero, Quaternion.identity, 0);
	}
	//This methods checks for the creation of signalling gameobjects and launches
	//the appropriate video.
	void Update(){
		//Launch video 3, destroy signalling gameobject
		if(GameObject.Find("RoofScene(Clone)") != null){
			Destroy(GameObject.Find("RoofScene(Clone)"));
			Application.LoadLevel("RoofScene");
		}
		//Launch video 4, destroy signalling gameobject
		if(GameObject.Find("BMXScene(Clone)") != null){
			Destroy(GameObject.Find("RoofScene(Clone)"));
			Application.LoadLevel("BMXScene");
		}
		//This checks if the player is the leader and disables video selection for the other player
		if(
			GameObject.Find("IamNotLeader(Clone)") &&
			!GameObject.Find("IamLeader(Clone)")
			){
				GameObject.Find("TLVideoButton").GetComponent<Button>().interactable = false;
				GameObject.Find("TRVideoButton").GetComponent<Button>().interactable = false;
				GameObject.Find("BLVideoButton").GetComponent<Button>().interactable = false;
				GameObject.Find("BRVideoButton").GetComponent<Button>().interactable = false;
				GameObject.Find("TourLeaderButton").GetComponent<Button>().interactable = false;
			}
	}
	//This function activates when the team leader button is selected, it creates
	//a local team leader object and a global not team leader object
	public void MakeLeader(){
		Debug.Log("I am Leader");
		Instantiate(Resources.Load("IamLeader"));
		Debug.Log("They are not Leader");
		PhotonNetwork.Instantiate("IamNotLeader", Vector3.zero, Quaternion.identity, 0);
	}
}
